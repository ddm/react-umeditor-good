'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _Dialog = require('../base/Dialog');

var _Dialog2 = _interopRequireDefault(_Dialog);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

//import './LinkDialog.less'

var LinkDialog = function (_Component) {
	_inherits(LinkDialog, _Component);

	function LinkDialog(props) {
		_classCallCheck(this, LinkDialog);

		var _this = _possibleConstructorReturn(this, (LinkDialog.__proto__ || Object.getPrototypeOf(LinkDialog)).call(this, props));

		_this.open = function (handle) {
			_this.setState({
				handle: handle
			});
			_this.refs.modal.open();
		};

		_this.close = function () {
			if (_this.refs.modal) {
				_this.refs.modal.close();
				if (_this.state.handle) {
					_this.state.handle();
				}
				_this.setState({
					link: {
						url: "",
						title: "",
						target: "__blank"
					}
				});
			}
		};

		_this.ok = function () {
			if (_this.state.link.url) {
				_this.state.handle(_this.state.link);
			}
			_this.close();
		};

		_this.toggle = function (handle) {
			_this.setState({
				handle: handle
			});
			_this.refs.modal.toggle();
		};

		_this.state = {
			link: {
				url: "",
				title: "",
				target: "__blank"
			},
			handle: function handle() {}
		};
		return _this;
	}

	_createClass(LinkDialog, [{
		key: 'updateLink',
		value: function updateLink(field, value) {
			var link = this.state.link;

			link[field] = value;
			this.setState({
				link: link
			});
		}
	}, {
		key: 'render',
		value: function render() {
			var _this2 = this;

			var link = this.state.link;

			var buttons = [{
				name: "btn-ok",
				content: "确定",
				onClick: this.ok
			}, {
				name: "btn-cancel",
				content: "取消",
				onClick: this.close
			}];
			if (this.props.hidden) {
				return _react2.default.createElement('div', null);
			} else {
				return _react2.default.createElement(
					_Dialog2.default,
					{
						ref: 'modal',
						className: 'link-dialog',
						width: 400,
						height: 282,
						title: '\u8D85\u94FE\u63A5',
						buttons: buttons,
						onClose: this.close },
					_react2.default.createElement(
						'table',
						{ className: 'edui-link-table' },
						_react2.default.createElement(
							'tbody',
							null,
							_react2.default.createElement(
								'tr',
								null,
								_react2.default.createElement(
									'td',
									null,
									_react2.default.createElement(
										'label',
										{ htmlFor: 'href' },
										'\u94FE\u63A5\u5730\u5740\uFF1A'
									)
								),
								_react2.default.createElement(
									'td',
									null,
									_react2.default.createElement('input', { value: link.url, className: 'edui-link-txt', id: 'edui-link-Jhref', type: 'text', onChange: function onChange(evt) {
											return _this2.updateLink('url', evt.target.value);
										} })
								)
							),
							_react2.default.createElement(
								'tr',
								null,
								_react2.default.createElement(
									'td',
									null,
									_react2.default.createElement(
										'label',
										{ htmlFor: 'title' },
										'\u6807\u9898\uFF1A'
									)
								),
								_react2.default.createElement(
									'td',
									null,
									_react2.default.createElement('input', { value: link.title, className: 'edui-link-txt', id: 'edui-link-Jtitle', type: 'text', onChange: function onChange(evt) {
											return _this2.updateLink('title', evt.target.value);
										} })
								)
							),
							_react2.default.createElement(
								'tr',
								null,
								_react2.default.createElement(
									'td',
									{ colspan: '2' },
									_react2.default.createElement(
										'label',
										{ htmlFor: 'target' },
										'\u662F\u5426\u5728\u65B0\u7A97\u53E3\u6253\u5F00\uFF1A'
									),
									_react2.default.createElement('input', { id: 'edui-link-Jtarget', type: 'checkbox', checked: link.target === '__blank', onChange: function onChange(evt) {
											return _this2.updateLink('target', evt.target.checked ? "__blank" : "");
										} })
								)
							)
						)
					)
				);
			}
		}
	}]);

	return LinkDialog;
}(_react.Component);

exports.default = LinkDialog;